"""
Print statements for terminal output
"""

import sys
from initial_conditions import J_func, h_func, T_func

def print_IC(J, h, T, L):
    if (len(J) == len(h) == len(T)):
        print()
        print("---INITIAL CONDITIONS---")
        
        if J_func == "const":
            print("Interaction parameter: %.02f"%J[0])
            print()
        else:
            print("Interaction parameter:")
            print(J)
            print()
        
        if h_func == "const":
            print("External magnetic field: %.02f"%h[0])
            print()            
        else:
            print("External magnetic field:")
            print(h)
            print()
        
        if T_func == "const":
            print("Temperature: %.02f"%T[0])
            print()        
        else:
            print("Temperature:")
            print(T)
            print()
            
        print("Starting lattice:")
        print(L)
        print()
        print("---PROGRESS---")
        print(' [0/%d]'%len(T), end='\r')

    else:
        print("ERROR - Initial condition vectors do not have the same length!")
        sys.exit()
        
def print_progress(i, J, h, T, mag, c, sus, run_time):
    print(' [%d/%d] --- J=%.02f, h=%.02f, T=%.02f --- mag=%.02f, c=%.02f, sus=%.02f --- time per iteration: %dh %dm %ds --- ETA: %dh %dm %ds ---  '%(i+1, len(T), J[i], h[i], T[i], mag[i], c[i], sus[i], run_time/(3600*(i+1)), (run_time/(60*(i+1)))%60, (run_time/(i+1))%60 , (len(T)-(i+1))*run_time/((i+1)*3600), ((len(T)-(i+1))*run_time/((i+1)*60))%60, ((len(T)-(i+1))*run_time/(i+1))%60), end='\r')
    
def print_results(mag, c, sus, L):
    print()
    print()
    print('---SIMULATION RESULTS---')
    print('Magnetisation:')
    print(mag)
    print()
    print('Specific heat:')
    print(c)
    print()
    print('Magnetic susceptibility:')
    print(sus)
    print()
    print('Final lattice:')
    print(L)
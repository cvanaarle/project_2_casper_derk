"""
Functions used for the bootstrap method:

resampled_distribution : creates a resampled distribution from the input vector
standard_deviation     : calculates the std
bootstrap              : calculates the std for the given inputs

"""

import numpy as np
from initial_conditions import flips_per_spin

# input: x = vector to be resampled, resample_itt = number of itterations taken for resampling
# output: resampled_dist: vector containing the mean of all resampled sets from the input vector
def resampled_distribution(x, resample_iterations):
    resampled_dist = np.zeros([resample_iterations])
    for i in range(resample_iterations):
        resampled_dist[i] = np.mean(np.random.choice(x, size=100*flips_per_spin, replace=True))
    
    return resampled_dist

# input: x = vector to be resampled, resampled_distribution = function written to calculate the above value, resample_itt = number of itterations taken for resampling
# output: std = standard deviation of the set
def standard_deviation(x):
    resample_iterations = 100
    re_dist = resampled_distribution(x, resample_iterations)
    std = np.sqrt(np.mean(re_dist**2)-(np.mean(re_dist))**2)
    return std

# input: Emem = energy of system for all MC steps, E2mem = energy^2 of system for all MC steps, magmem = magnetisation of system for all MC steps, mag2mem = magnetisation^2 of system for all MC steps, magmemabs = absolute magnetisation  of system for all MC steps
# output: Emem_std = std ofenergy of system, E2mem_std = std of energy^2 of system, magmem_std = std of magnetisation of system, mag2mem_std = std of magnetisation^2 of system, magmemabs_std = std of absolute magnetisation  of system
def bootstrap(Emem, E2mem, magmem, mag2mem, magmemabs):
    Emem_std = standard_deviation(Emem)
    E2mem_std = standard_deviation(E2mem)
    magmem_std = standard_deviation(magmem)
    mag2mem_std = standard_deviation(mag2mem)
    magmemabs_std = standard_deviation(magmemabs)
    return Emem_std, E2mem_std, magmem_std, mag2mem_std, magmemabs_std
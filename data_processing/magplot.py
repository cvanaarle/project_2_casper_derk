""" 
Makes plots for each intermediate temperature step
"""

import PyQt5
import matplotlib
matplotlib.use("Agg")
import numpy as np
import matplotlib.pyplot as plt

from initial_conditions import xh_ym, xh_ysus, xT_ym, xT_yc, xT_ysus, xT_ymabs, xT_ytime

def magplot(T, J, h, mag, magabs, c, sus, it_time, mag_std, c_std, sus_std, magabs_std):
    
    if (xh_ym == True):
        plt.figure()
        plt.errorbar(h, mag, yerr=mag_std, fmt='.b', capsize=5)
        plt.savefig('images/xh_ym.png')
        plt.close('all')
        
    if (xh_ysus == True):
        plt.figure()
        plt.errorbar(h, sus, yerr=sus_std, fmt='.b', capsize=5)
        plt.savefig('images/xh_ysus.png')
        plt.close('all')
        
    if (xT_ym == True):
        plt.figure()
        plt.errorbar(T, mag, yerr=mag_std, fmt='.b', capsize=5)
        plt.savefig('images/xT_ym.png')
        plt.close('all')
        
    if (xT_yc == True):
        plt.figure()
        plt.errorbar(T, c, yerr=c_std, fmt='.b', capsize=5)
        plt.savefig('images/xT_yc.png')
        plt.close('all')
        
    if (xT_ysus == True):
        plt.figure()
        plt.errorbar(T, sus, yerr=sus_std, fmt='.b', capsize=5)
        plt.savefig('images/xT_ysus.png')
        plt.close('all')
    
    if (xT_ymabs == True):
        plt.figure()
        plt.errorbar(T, magabs, magabs_std, fmt='.b', capsize=5)
        plt.savefig('images/xT_ymabs.png')
        plt.close('all')
        
    if (xT_ytime == True):
        plt.figure()
        plt.plot(T, it_time, '.b')
        plt.savefig('images/xT_ytime.png')
        plt.close('all')
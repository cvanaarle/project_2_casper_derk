"""
Saves data to text files
"""

import numpy as np

def magtxt(T, J, h, mag, magabs, c, sus, it_time, mag_std, c_std, sus_std, magabs_std):
    np.savetxt('txtdata/Temperature.txt', (T) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Temperature"]))
    np.savetxt('txtdata/InteractionParameter.txt', (J) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Interaction Parameter"]))
    np.savetxt('txtdata/MagneticField.txt', (h) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "External Magnetic Field"]))
    np.savetxt('txtdata/Magnetisation.txt', (mag) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetisation"]))
    np.savetxt('txtdata/Magnetisation_absolute.txt', (magabs) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Absolute Magnetisation"]))
    np.savetxt('txtdata/SpecificHeat.txt', (c) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "InteractionParameter"]))
    np.savetxt('txtdata/Susceptibility.txt', (sus) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetic Susceptibility"]))
    np.savetxt('txtdata/IterationTime.txt', (it_time) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Simulation time for each iteration"]))
    
    np.savetxt('txtdata/Magnetisation_Std.txt', (mag_std) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetisation"]))
    np.savetxt('txtdata/Magnetisation_absolute_Std.txt', (magabs_std) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetisation"]))
    np.savetxt('txtdata/SpecificHeat_Std.txt', (c_std) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetisation"]))
    np.savetxt('txtdata/Susceptibility_Std.txt', (sus_std) , delimiter=',',newline = '\n' , header='\n'.join(["Text file containing the following observable:", "Magnetisation"]))

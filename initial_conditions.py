"""
Here the initial configuration is set.

parameters set:
simtype           : algorithm type (conv = Metropolis, sw = Swendsen-Wang, wolff = Wolff)
data_points       : number of data points gathered for the plots
n                 : number of cells along one side of the 2d lattice
filps_per_spin    : amount of flips per spin
flips_till_stable : the amount of flips per spin before the system to start collecting data
spin_distribution : initial state of spins (rand = uniform random distribution, pos = all spin up, neg = all spin down)
J_func            : function type to vary J through the simulation
Jmax              : maximum value of J used in J_func generation
h_func            : function type to vary h through the simulation
hmax              : maximum value of h used in h_func generation
T_func            : function type to vary T through the simulation
Tmax              : maximum value of T used in T_func generation

x?1?_y?2?         : plot types with ?1? indicating the quantity along x and ?2? the quantity along y. Specifications are given below.

J                 : Vector with the coupling constant
h                 : Vector with the external magnetic field
T                 : Vector with al temperature values

M                 : number of Montecarlo steps
neighbour_vector  : sum of spins in neigbouring cells
L                 : lattice with distribution of spins (rand = randomly distributed spins, pos = spins with values 1, neg = spins with values -1 

"""

import numpy as np

#Simulation types: "conv", "sw", "wolff"
simtype = "conv"
data_points = 160
n = 5
flips_per_spin = 20000     
flips_till_stable = 5000   

#Available distributions: "rand", "pos", "neg"
spin_distribution = "rand" 

#Available distributions: "const", "lin", "man"
J_func = "const"
Jmax = 1
h_func = "const"
hmax = 0
T_func = "man"
Tmax = 5

#available plot types: "xh_ym", "xh_ysus", "xT_ym", "xT_yc", "xT_ysus", "xT_mabs" 
xh_ym      = True
xh_ysus    = True
xT_ym      = True
xT_yc      = True
xT_ysus    = True
xT_ymabs   = True
xT_ytime   = True


#initial conditions for J vector
if J_func == "const":
    J = Jmax * np.ones(data_points)

if J_func == "lin":
    J = np.linspace(Jmax/data_points,Jmax, num=data_points)
    
if J_func == "man":
    J = np.array([])


#initial conditions for h vector
if h_func == "const":
    h = hmax *np.ones(data_points)
    
if h_func == "lin":
    h = np.linspace(hmax/data_points, hmax, num=data_points)
    
if h_func == "man":
    h = 0.1 * np.array([2, 1.8, 1.6, 1.4, 1.2, 1.0, 0.8, 0.6, 0.4, 0.2, 0, -0.2, -0.4, -0.6, -0.8, -1.0, -1.2, -1.4, -1.6, -1.8, -2, -1.8, -1.6, -1.4, -1.2, -1.0, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2])


#initial conditions for T vector
if T_func == "const":
    T = Tmax * np.ones(data_points)
    
if T_func == "lin":
    T = np.linspace(Tmax,Tmax/data_points,num=data_points)

if T_func == "man":
    T0 = np.linspace(Tmax,3.55,30)
    T1 = np.linspace(3.5,1.52,100)
    T2 = np.linspace(1.5,0.05, 30)
    T = np.concatenate([T0, T1, T2])

if simtype == "conv":
    M = flips_per_spin*n**2
    
if simtype == "sw":
    M = flips_per_spin
    
if simtype == "wolff":
    M = flips_per_spin*n**2
    
neighbour_vector = [4,2,0,-2,-4]

if spin_distribution == "rand":
    L01 = np.int8(np.around(np.random.rand(n,n)))
    L = np.where(L01==0, -1, L01)
    
if spin_distribution == "pos":
    L = np.ones((n,n))
    
if spin_distribution == "neg":
    L = -1*np.ones((n,n))









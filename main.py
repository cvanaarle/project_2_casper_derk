"""
This runs the simulation. 
For input parameters we refer to initial_conditions.py
For output files we refer to the folders images and txtdata

The code is split in 3 parts, each refering to one of the three algorithms
"""

import numpy as np
import sys
import time

from cosmetics import *

from initial_conditions import simtype, n, M, L, T, neighbour_vector, J, h
from simulation.data.data_functions import *
from simulation.observables import *
from simulation.adaptions import *

from data_processing.magplot import *
from data_processing.magtxt import *

# Metropolis algorithm
if simtype == "conv": 
    
    print_IC(J, h, T, L)
    it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std = observable_initialisation(T)
    start_time = time.time()
    run_time = 0
    
    for i in range(len(T)): # loop over temperature vector
        energy_change = energy_change_gen(J[i],h[i])
        accept_prob = np.exp(-np.where(energy_change<0, 0, energy_change)/T[i])
        Emem, E2mem, magmem, mag2mem, magmemabs = data_initialisation(M, L, n, J[i], h[i], neighbour_vector) 

        for m in range(M): # loop over spinflips per site
            index1 = np.random.randint(0,n) # random x coordinate
            index2 = np.random.randint(0,n) # random y coordinate
            s_i, position, flipfactor = lattice_update(n, L, index1, index2, neighbour_vector, accept_prob)
            L, Emem, E2mem, magmem, mag2mem, magmemabs = data_write(m, L, energy_change, index1, index2, s_i, position, flipfactor, Emem, E2mem, magmem, mag2mem, magmemabs)

        run_time_old = run_time
        run_time = time.time() - start_time
        it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std = observable_write(i, it_time, mag, magabs, c, sus, mag_std, c_std, sus_std, magabs_std, n, M, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old)
        
        # updating plots and textfiles
        magplot(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])
        magtxt(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])

        print_progress(i, J, h, T, mag, c, sus, run_time)

    print_results(mag, c, sus, L)

# Swendsen-Wang algorithm
if simtype == "sw":
    
    print_IC(J, h, T, L)
    it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std = observable_initialisation(T)
    start_time = time.time()
    run_time = 0
    
    for i in range(len(T)): # loop over temperature vector
        p = 1 - np.exp(-2 * J[i] / T[i])
        Emem, E2mem, magmem, mag2mem, magmemabs = data_initialisation(M, L, n, J[i], h[i], neighbour_vector)
        sw_chance_vector = sw_chance(n, h[i])
        
        for m in range(M): # loop over spinflips per site
            bonds_h, bonds_v = sw_bonds(n, L, p)
            sw_cluster_matrix = np.zeros((n,n))
            sw_cluster_matrix = sw_cluster(n, sw_cluster_matrix, bonds_h, bonds_v)
            L = sw_flip(sw_cluster_matrix, sw_chance_vector, L) # updated lattice
            Emem, E2mem, magmem, mag2mem = data_write_sw(i, m, n, L, J, h, neighbour_vector, Emem, E2mem, magmem, mag2mem)
        
        run_time_old = run_time
        run_time = time.time() - start_time
        it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std = observable_write_sw(i, it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std, n, M, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old)
        
        # updating plots and textfiles
        magplot(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])
        magtxt(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])
        
        print_progress(i, J, h, T, mag, c, sus, run_time)
    
    print_results(mag, c, sus, L)

# Wolff algorithm    
if simtype == "wolff":
       
    print_IC(J, h, T, L)
    it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std = observable_initialisation(T)
    start_time = time.time()
    run_time = 0

    for i in range(len(T)): # loop over temperature vector
        p = 1 - np.exp(-2 * J[i] / T[i])
        Emem, E2mem, magmem, mag2mem, magmemabs = data_initialisation(M, L, n, J[i], h[i], neighbour_vector)
        m = 0
        Mcounter = 0
        Mfinder = np.zeros(M+1)
        
        while Mcounter < M: # loop over spinflips per site
            index1 = np.random.randint(0,n) # random x coordinate
            index2 = np.random.randint(0,n) # random y coordinate
            L, clustersize = wolff_cluster(index1, index2, n, L, p)
            Emem, E2mem, magmem, mag2mem = data_write_wolff(i, m, n, L, J, h, neighbour_vector, Emem, E2mem, magmem, mag2mem)
            #counters to determine the number of iterations
            Mcounter = Mcounter + clustersize
            Mfinder[m+1] = Mcounter
            m = m+1

        run_time_old = run_time
        run_time = time.time() - start_time
        it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std = observable_write_wolff(i, it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std, n, m, Mfinder, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old)
        
        # updating plots and textfiles
        magplot(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])
        magtxt(T[0:i+1], J[0:i+1], h[0:i+1], mag[0:i+1], magabs[0:i+1], c[0:i+1], sus[0:i+1], it_time[0:i+1], mag_std[0:i+1], c_std[0:i+1], sus_std[0:i+1], magabs_std[0:i+1])
        
        print_progress(i, J, h, T, mag, c, sus, run_time)
    
    print_results(mag, c, sus, L)

            
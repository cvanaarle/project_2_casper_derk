""" 
Functions used in updating the Lattice

Ham               : calculates the Hamiltonian for the given spin and it's nearest neighbours
energy_change_gen : the change in energy when flipping a single spin for the 10 different situations for a given J,h combination 
lattice_update    : determines the direction and position of spin that needs to be flipped. Also determines whether it needs to be flipped.
flip              : a weighted toincoss with chance p getting a spin flip and 1-p that it remains.

sw_bonds          : creates bond structure (already including deleted bonds)
sw_cluster        : Labels all clusters
sw_flip           : updates the lattice by flipping the clusters with chance p
sw_chance         : creates vector with flip probabilities for all different cluster sizes

wolff_cluster     : updates the lattice by flipping an entire cluster
wolff_update      : updates the lattice by flipping the top spin in the stack

"""

import numpy as np

# input: s_i = spin i, s_j = neighbouring spin values, J = lattice constant, h = external magnetic field
# output: H = Hamiltonian
def Ham(s_i,s_j,J,h):
    H = -J*s_i*np.sum(s_j)-h*s_i
    return H

# input: J = lattice constant, h = external magnetic field
# output: energy_change = matrix containing the energy change for the 10 different configurations
def energy_change_gen(J,h):
    energy_change = np.zeros([2,5])
    s_i = [1, -1]
    s_j = np.matrix('1,1,1,1; 1,1,1,-1; 1,1,-1,-1; 1,-1,-1,-1; -1,-1,-1,-1')

    for i in range(2):
        for j in range(5):
            energy_change[i,j] = Ham(-1*s_i[i],s_j[j,:],J,h)-Ham(s_i[i],s_j[j,:],J,h)
    
    return energy_change

# input: n = lattice size, L = lattice, index1 = x-coördinate of spin i, index2 = y-coördinate of spin i, neighbour_vector = sum of spins in neigbouring cells, accept_prob = acceptance probability matrix for the 10 different configuration
# output : s_i = spin of particle i, position = position of sum of nearest neighbours in neigbour_vector, flipfactor = determines if spin i is flipped.
def lattice_update(n, L, index1, index2, neighbour_vector, accept_prob):
	s_i = L[index1, index2] 
	s_j = L[index1-1, index2] + L[(index1+1)%n, index2] + L[index1, index2-1] + L[index1, (index2+1)%n] # sum of nearest neighbours
	positioncheck = np.where(neighbour_vector == s_j)
	position = int(positioncheck[0])  
	flip_prob =  accept_prob[int((s_i-1)/2),position] # probability of flipping
	flipfactor = flip(flip_prob) # +1 if stays -1 if flips
	return s_i, position, flipfactor

# input: p = flip probability
# output: fl = 1 if spin doesn't flip and -1 if spin flips
def flip(p):
    a = np.random.random_sample() 
    fl = (a-p)/abs(a-p)
    return fl


# input: n = lattice size, L = lattice, p = chance of bond remaining
# output: bonds_h = n*n matrix conaining horizontal bonds, bonds_v = n*n matrix containing vertical bonds
#First find all possible bonds, then multiply with chance to freeze bond, then add random scalar and downcast to int.
def sw_bonds(n, L, p):
    bonds_h = np.int_((p*(   np.absolute(0.5 * (L + np.roll(L,1,axis=1)))   )) + np.random.rand(n,n))
    bonds_v = np.int_((p*(   np.absolute(0.5 * (L + np.roll(L,1,axis=0)))   )) + np.random.rand(n,n))
    return bonds_h, bonds_v

# input: n = lattice size, sw_cluster_matrix = matrix containing cluster labels, bonds_h = n*n matrix conaining horizontal bonds, bonds_v = n*n matrix containing vertical bonds
# output: sw_cluster_matrix = matrix containing cluster labels
def sw_cluster(n, sw_cluster_matrix, bonds_h, bonds_v):
    cn = 1
    sw_cluster_matrix[0,0] = cn
    
    #Fill first row with cluster labels
    for j in range(1,n):
        if bonds_h[0,j] == 1:
            sw_cluster_matrix[0,j] = cn
        else:
            cn += 1
            sw_cluster_matrix[0,j] = cn
    
    #Fill first column with cluster labels
    for i in range(1,n):
        if bonds_v[i,0] == 1:
            sw_cluster_matrix[i,0] = sw_cluster_matrix[i-1,0]
        else:
            cn += 1
            sw_cluster_matrix[i,0] = cn
    
    #Fill rest of volume with cluster labels
    for i in range(1,n):
        for j in range(1,n):
            if bonds_v[i,j] == 1:
                sw_cluster_matrix[i,j] = sw_cluster_matrix[i-1,j]
                if bonds_h[i,j] == 1:
                    if sw_cluster_matrix[i-1,j] != sw_cluster_matrix[i,j-1]:
                        sw_cluster_matrix = np.where(sw_cluster_matrix == sw_cluster_matrix[i,j-1], sw_cluster_matrix[i-1,j], sw_cluster_matrix)
            elif bonds_h[i,j] == 1:
                sw_cluster_matrix[i,j] = sw_cluster_matrix[i,j-1]
            else:
                cn += 1
                sw_cluster_matrix[i,j] = cn
    
    #test for clusters across boundaries
    for j in range(n):
        if bonds_v[0,j] == 1:
            if sw_cluster_matrix[0,j] != sw_cluster_matrix[-1,j]:
                sw_cluster_matrix = np.where(sw_cluster_matrix == sw_cluster_matrix[-1,j], sw_cluster_matrix[0,j], sw_cluster_matrix)
                
    for i in range(n):
        if bonds_h[i,0] == 1:
            if sw_cluster_matrix[i,0] != sw_cluster_matrix[i,-1]:
                sw_cluster_matrix = np.where(sw_cluster_matrix == sw_cluster_matrix[i,-1], sw_cluster_matrix[i,0], sw_cluster_matrix)
                
    return sw_cluster_matrix

# input: sw_cluster_matrix = matrix containing cluster labels, sw_chance_vector = vector conaining flip probability for all different cluster sizes, L = lattice
# output: L = updated lattice
def sw_flip(sw_cluster_matrix, sw_chance_vector, L):
    for i in np.unique(sw_cluster_matrix):
        cluster_size = np.where(sw_cluster_matrix==i,1,0).sum()
        update = int(sw_chance_vector[cluster_size - 1] + np.random.random_sample()) * 2 - 1
        L = np.where(sw_cluster_matrix == i, update ,L)
    
    return L

# input: n = lattice size, h = external magnetic field
# output: chance = vector containing flip probability for all different cluster sizes
def sw_chance(n, h):
    exponent = np.exp(-2*np.arange(1,n**2+1)*h)
    chance = 1/(exponent + 1)
    return chance

# input: index1 = x coördinate of spin i, index2 = y coördinate of spin i, n = lattice size, L = lattice, p = probability of forming a bond
# output: L = updated lattice, clustersize = size of the flipped cluster
def wolff_cluster(index1, index2, n, L, p):
    s_i = L[index1, index2] #spin of particle i
    L[index1, index2] = -1*s_i
    stack =  np.array([[index1, index2]])
    clustersize = 1
    while stack.size > 0:
        L, stack = wolff_update(n, L, stack, s_i, p)
        clustersize = clustersize + 1
    return L, clustersize

# input: n = lattice size, L = lattice, stack = stack keeping track of spin locations belonging to cluster, s_i = spin of particle i, p = probability of forming a bond
# output: L = updated lattice, stack = updated stack
def wolff_update(n, L, stack, s_i, p):
    index1 = stack[0,0]
    index2 = stack[0,1]
    bondfactor = flip(p)
    if L[(index1-1)%n, index2] == s_i and bondfactor == -1:
        L[(index1-1)%n, index2] = -1*L[(index1-1)%n, index2]
        new = np.array([[(index1-1)%n, index2]])
        stack = np.concatenate((stack, new), axis=0)
    bondfactor = flip(p)
    if L[index1, (index2+1)%n] == s_i and bondfactor == -1:
        L[index1, (index2+1)%n] = -1*L[index1, (index2+1)%n]
        new = np.array([[index1, (index2+1)%n]])
        stack = np.concatenate((stack, new), axis=0)
    bondfactor = flip(p)
    if L[(index1+1)%n, index2] == s_i and bondfactor == -1:
        L[(index1+1)%n, index2] = -1*L[(index1+1)%n, index2]
        new = np.array([[(index1+1)%n, index2]])
        stack = np.concatenate((stack, new), axis=0)  
    bondfactor = flip(p)
    if L[index1, (index2-1)%n] == s_i and bondfactor == -1:
        L[index1, (index2-1)%n] = -1*L[index1, (index2-1)%n]
        new = np.array([[index1, (index2-1)%n]])
        stack = np.concatenate((stack, new), axis=0)
    
    stack = np.delete(stack, 0, 0)
    return L, stack
                
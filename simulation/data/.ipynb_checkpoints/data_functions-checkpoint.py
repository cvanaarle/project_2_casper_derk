"""
Fucntions for initialising data and writing new data
"""

import numpy as np

def data_initialisation(M,L):
    Emem = np.zeros(M+1)
    magmem = np.zeros(M+1)
    cmem = np.zeros(M+1)
    Lmem = L
    
    magmem[0] = L.sum()
    
    return Lmem, Emem, magmem, cmem

def data_write(m, Lmem, index1, index2, flipfactor, Emem, Enew, magmem, magnew, cmem, cnew):
    Lmem[index1-1,index2-1] = Lmem[index1-1,index2-1]*flipfactor
    Emem[m+1] = Enew
    magmem[m+1] = magnew
    cmem[m+1] = cnew
    return Lmem, Emem, magmem, cmem
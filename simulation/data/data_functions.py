"""
Fucntions for initialising data and writing new data:

data_initialisation       : initialises the memory data
data_write                : writes the m+1 timestep tot the memory
observable_initialisation : initialises the observables
observable_write          : writes the i'th temperature observable values
data_write_sw             : writes the data from the Swendsen-Wang algorithm
observable_write_sw       : writes the observables from the Swendsen-Wang algorithm
data_write_wolff          : writes the data from the Wolff algorithm
observable_write_wolff    : writes the observables from the Wolff algorithm

Used input/output:
M            : amount of Monte Carlo steps
L            : lattice
n            : lattice size
J            : coupling constant
h            : external magnetic field
neigbour_vector : vector containing sum over spins of nearest neighbours

Emem         : energy of system
E2mem        : energy^2 of system
magmem       : magnetisation of system
mag2em       : magnetisation^2 of system
magmemabs    : absolute value of magnetisation

Emem_std     : std of energy of system
E2mem_std    : std of energy^2 of system 
magmem_std   : std of magnetisation of system
mag2mem_std  : std of magnetisation^2 of system 
magmemabs_std : std of absolute magnetisation  of system 

m            : Monte Carlo step
L            : lattice 
energy_change : change in energy
index1       : x coördinate of spin i
index2       : y coördinate of spin i
s_i          : spin of spin i
position     : position in energy_change matrix
flipfactor   : multiplication factor if spin will flip. 1 if it doesn't flip, -1 if it flips.

T            : Temperature vector
it_time      : time it takes per iteration
mag          : timeaveraged magnetization
c            : timeaveraged specific heat
sus          : timeaveraged magnetic susceptibility
magabs       : timeaveraged absolute value of magnetization
mag_std      : std of timeaveraged magnetization
c_std        : std of timeaveraged specific heat
sus_std      : std of timeaveraged magnetic susceptibility
magabs_std   : std oftimeaveraged absolute value of magnetization
    
run_time     : time elapsed since start
run_time_old : previous time elapsed since start


"""

import numpy as np
from simulation.adaptions import flip, Ham
from simulation.observables import *
from initial_conditions import flips_till_stable
from data_processing.bootstrap import *
   
def data_initialisation(M, L, n, J, h, neighbour_vector):
    Etemp = energy_lattice(n, L, J, h, neighbour_vector)
    
    E2temp = np.square(Etemp)
    
    Emem = np.zeros(M+1)
    E2mem = np.zeros(M+1)
    magmem = np.zeros(M+1)
    mag2mem = np.zeros(M+1)
    magmemabs = np.zeros(M+1)

    Emem[0] = Etemp.sum()
    E2mem[0] = Emem[0]**2
    magmem[0] = L.sum()
    mag2mem[0] = magmem[0]**2
    
    return Emem, E2mem, magmem, mag2mem, magmemabs

def data_write(m, L, energy_change, index1, index2, s_i, position, flipfactor, Emem, E2mem, magmem, mag2mem, magmemabs):
    L[index1, index2] = L[index1, index2]*flipfactor
    Emem[m+1], E2mem[m+1] = energy(Emem, E2mem, m, s_i, flipfactor, energy_change, position)
    magmem[m+1], mag2mem[m+1] = magnetization(magmem, m, s_i, flipfactor)
    magmemabs[m+1] = abs(magmem[m+1])
    return L, Emem, E2mem, magmem, mag2mem, magmemabs

def observable_initialisation(T):
    it_time = np.zeros(len(T))
    mag = np.zeros(len(T))
    c = np.zeros(len(T))
    sus = np.zeros(len(T))
    magabs = np.zeros(len(T))
    
    mag_std = np.zeros(len(T))
    c_std = np.zeros(len(T))
    sus_std = np.zeros(len(T))
    magabs_std = np.zeros(len(T))
    return it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std
    
def observable_write(i, it_time, mag, magabs, c, sus, mag_std, c_std, sus_std, magabs_std, n, M, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old):
    it_time[i] = run_time - run_time_old
    mag[i] = np.average(magmem[flips_till_stable*n**2:M])/n**2    #indices scale with monte carlo length
    c[i] = (np.average(E2mem[flips_till_stable*n**2:M]) - np.average(Emem[flips_till_stable*n**2:M])**2)/(n**2 * T[i]**2)        # ""
    sus[i] = (np.average(mag2mem[flips_till_stable*n**2:M]) - np.average(magmemabs[flips_till_stable*n**2:M])**2)/(n**2 * T[i])
    magabs[i] = np.average(magmemabs[flips_till_stable*n**2:M])/n**2
    
    Emem_std, E2mem_std, magmem_std, mag2mem_std, magmemabs_std = bootstrap(Emem[flips_till_stable*n**2:M], E2mem[flips_till_stable*n**2:M], magmem[flips_till_stable*n**2:M], mag2mem[flips_till_stable*n**2:M], magmemabs[flips_till_stable*n**2:M])
    
    mag_std[i] = magmem_std
    c_std[i] = (E2mem_std + 2 * np.average(Emem[flips_till_stable*n**2:M]) * Emem_std)/(n**2 * T[i]**2)
    sus_std[i] = (mag2mem_std + 2 * np.average(magmemabs[flips_till_stable*n**2:M]) * magmemabs_std)/(n**2 * T[i])
    magabs_std[i] = magmemabs_std
    
    return it_time, mag, c, sus, magabs, mag_std, c_std, sus_std, magabs_std

def data_write_sw(i, m, n, L, J, h, neighbour_vector, Emem, E2mem, magmem, mag2mem):
    Emem[m+1] = energy_lattice(n, L, J[i], h[i], neighbour_vector).sum()
    E2mem[m+1] = Emem[m+1]**2
    magmem[m+1] = np.absolute(L.sum())
    mag2mem[m+1] = magmem[m+1]**2
    return Emem, E2mem, magmem, mag2mem

def observable_write_sw(i, it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std, n, M, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old):
    it_time[i] = run_time - run_time_old
    mag[i] = np.average(magmem[flips_till_stable:M])/n**2
    c[i] = (np.average(E2mem[flips_till_stable:M]) - np.average(Emem[flips_till_stable:M])**2)/(n**2 * T[i]**2)
    sus[i] = (np.average(mag2mem[flips_till_stable:M]) - np.average(magmem[flips_till_stable:M])**2)/(n**2 * T[i])

    Emem_std, E2mem_std, magmem_std, mag2mem_std, magmemabs_std = bootstrap(Emem[flips_till_stable:M], E2mem[flips_till_stable:M], magmem[flips_till_stable:M], mag2mem[flips_till_stable:M], magmemabs[flips_till_stable:M])
    
    mag_std[i] = magmem_std
    c_std[i] = (E2mem_std + 2 * np.average(Emem[flips_till_stable:M]) * Emem_std)/(n**2 * T[i]**2)
    sus_std[i] = (mag2mem_std + 2 * np.average(magmem[flips_till_stable:M]) * magmem_std)/(n**2 * T[i])
    magabs_std[i] = 0
    
    return it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std

def data_write_wolff(i, m, n, L, J, h, neighbour_vector, Emem, E2mem, magmem, mag2mem):
    Emem[m+1] = energy_lattice(n, L, J[i], h[i], neighbour_vector).sum()
    E2mem[m+1] = Emem[m+1]**2
    magmem[m+1] = np.absolute(L.sum())
    mag2mem[m+1] = magmem[m+1]**2
    return Emem, E2mem, magmem, mag2mem

def observable_write_wolff(i, it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std, n, m, Mfinder, T, Emem, E2mem, magmem, mag2mem, magmemabs, run_time, run_time_old):
    it_time[i] = run_time - run_time_old
    mag[i] = np.average(magmem[np.argmax(Mfinder>flips_till_stable):m-1])/n**2
    c[i] = (np.average(E2mem[np.argmax(Mfinder>flips_till_stable):m-1]) - np.average(Emem[np.argmax(Mfinder>flips_till_stable):m-1])**2)/(n**2 * T[i]**2)
    sus[i] = (np.average(mag2mem[np.argmax(Mfinder>flips_till_stable):m-1]) - np.average(magmem[np.argmax(Mfinder>flips_till_stable):m-1])**2)/(n**2 * T[i])
    
    Emem_std, E2mem_std, magmem_std, mag2mem_std, magmemabs_std = bootstrap(Emem[np.argmax(Mfinder>flips_till_stable):m-1], E2mem[np.argmax(Mfinder>flips_till_stable):m-1], magmem[np.argmax(Mfinder>flips_till_stable):m-1], mag2mem[np.argmax(Mfinder>flips_till_stable):m-1], magmemabs[np.argmax(Mfinder>flips_till_stable):m-1])
    
    mag_std[i] = magmem_std
    c_std[i] = (E2mem_std + 2 * np.average(Emem[np.argmax(Mfinder>flips_till_stable):m-1]) * Emem_std)/(n**2 * T[i]**2)
    sus_std[i] = (mag2mem_std + 2 * np.average(magmem[np.argmax(Mfinder>flips_till_stable):m-1]) * magmem_std)/(n**2 * T[i])
    magabs_std[i] = 0
    
    return it_time, mag, c, sus, mag_std, c_std, sus_std, magabs_std
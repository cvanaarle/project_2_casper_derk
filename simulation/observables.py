""" 
Functions for observables

magnetization : calculates the total magnetization at timestep m+1
energy : calculates the energy and energy^2 at timestep m+1
energy_lattice : calculates the energy of the total lattice

"""
import numpy as np
from simulation.adaptions import Ham

# input: magmem = magnetization from memory, m = Montecarlo step, s_i = spin value of spin i, flipfactor = multiplactor factor (1 if spin remains, -1 if spin flips)
# output: magnew = new magnetization, mag2new = new magnetization sqaure
def magnetization(magmem, m, s_i, flipfactor):
    magnew = magmem[m]+(flipfactor-1)*s_i
    mag2new = magnew**2
    return magnew, mag2new
    
# input: Emem = energy from memory, E2mem = energy squared from memory, m = Montecarlo step, s_i = spin value of spin i, flipfactor = multiplactor factor (1 if spin remains, -1 if spin flips), energy_change = change in energy for the spinflip, position = position in energy_change matrix
# output: Enew = new energy of lattice, E2new = new energy squared of the lattice
def energy(Emem, E2mem, m, s_i, flipfactor, energy_change, position):
    dE = -energy_change[int((-s_i+1)/2+0.5),position]*(flipfactor-1)
    Enew = Emem[m] + dE
    E2new = Enew**2
    return Enew, E2new

# input: n = lattice size, L = lattice, J = coupling constant, h = external magnetic field, neighbour_vector = vector containing sum over spins of nearest neighbours
# output: E_lattice = energy of lattice
def energy_lattice(n, L, J, h, neighbour_vector):
    energy_set = np.zeros([2,5]) # set of possible energy configurations
    s_i = [1, -1]
    s_j = np.matrix('1,1,1,1; 1,1,1,-1; 1,1,-1,-1; 1,-1,-1,-1; -1,-1,-1,-1')
    for i in range(2):
        for j in range(5):
            energy_set[i,j] = Ham(s_i[i],s_j[j,:],J,h)
            
    E_lattice = np.zeros([n, n])
    for i in range(n):
        for j in range(n):
            s_i = L[i, j] # spin of particle i
            s_j = L[i-1, j]+ L[(i+1)%n, j]+ L[i, j-1] + L[i, (j+1)%n] # sum of nearest neighbours
            positioncheck = np.where(neighbour_vector == s_j) # position of sum of nearest neighbours in neigbour_vector
            position = int(positioncheck[0])
            E_lattice[i,j] = energy_set[int((s_i-1)/2),position]
    
    return E_lattice
